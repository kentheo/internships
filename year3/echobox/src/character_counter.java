/**
 * Created by kendeas93 on 04/12/16.
 */


import jdk.internal.util.xml.impl.ReaderUTF8;
import jdk.nashorn.internal.runtime.regexp.joni.Regex;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import java.util.*;
import java.util.regex.*;

public class character_counter {

    /*
    It is important to note that you won’t necessarily know the length of a t.co URL before posting.
    Consider the short_url_length field a maximum possible length for a t.co-wrapped link.
    When designing a Tweet entry box, it’s best to consider all URLs as equalling the maximum possible short_url_length.
     */


    /*
    private static BufferedReader input;
    private static int peeked = -1;
    
    private static char peek() throws IOException {
        if (peeked != -1)
            return (char)peeked;

        int r = input.read();
        if (r == -1)
            throw new IOException();

        peeked = r;
        return (char) r;
    }


    private static char next() throws IOException {
        char r;
        if (peeked != -1) {
            r = (char) peeked;
            peeked = -1;
        } else {
            int i = input.read();
            if (i == -1)
                throw new IOException();
            r = (char) i;
        }
        return r;
    }

    Tried using Lexing into tokens as I did in my Compiling Techniques course but it's not the best way.
    Costed a lot of time.

    */

    // Use Regex to find a pattern for URLs
    public static final String URL_REGEX = "^((https?)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";
    private static List<String> findURLs(String string) {
        List<String> urls = new ArrayList<>();
        String[] parts = string.split("\\s+");
        Pattern pattern = Pattern.compile(URL_REGEX, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
        for (String item : parts) {
            if (pattern.matcher(item).matches()){
                urls.add(item);
            }
        }

        return urls;
    }

    private static int getCount(String stringToCount) {
        //return the number of “effective” characters this tweet corresponds with.
        // Ultimately, if the length of a URL is greater than 23, then t.co kicks in and alters the length
        int upper_limit = 140;
        int short_url_length = 23;      // same as short_url_length_https
//        int short_url_length_https = 23;

        stringToCount = Normalizer.normalize(stringToCount, Normalizer.Form.NFC);
        int stringLength = stringToCount.codePointCount(0, stringToCount.length());

        List<String> urls = findURLs(stringToCount);
        for (String url : urls ){
            if (url.length() > short_url_length){
                stringLength += short_url_length - url.length();
            }
        }

        return stringLength;
    }

    public static void main(String[] args) {
        String provided_test = "Former UNM law professor named FERC chair ​ http://bit.ly/1IifCDF";
        String provided_test1= "Former UNM law professor named FERC chair \u200B http://bit.ly/1IifCDlkjsdfglkjsdgsdfgsdfglkjsdfglkjsdgF";
        String url_check = "http://www.google/lkdfg/fd";
        List<String> test_url = findURLs(provided_test);
        System.out.println(getCount(provided_test));
        for (String url : test_url){
            System.out.println(url);
        }


    }
}
