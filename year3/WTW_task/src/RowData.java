/**
 *
 * Created by kendeas93 on 16/03/17.
 */
public class RowData {

    private String product;
    int origin_year;
    int develop_year;
    double increm_value;

    public RowData(String product, int origin_year, int develop_year, double increm_value) {
        this.product = product;
        this.origin_year = origin_year;
        this.develop_year = develop_year;
        this.increm_value = increm_value;
    }

    public String getProduct() {
        return product;
    }

    public int getDevelop_year() {
        return develop_year;
    }

    public int getOrigin_year() {
        return origin_year;
    }

    public double getIncrem_value() {
        return increm_value;
    }

    @Override
    public String toString() {
        return "RowData{" +
                "product='" + product + '\'' +
                ", origin_year=" + origin_year +
                ", develop_year=" + develop_year +
                ", increm_value=" + increm_value +
                '}';
    }
}
