import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 *
 * Created by kendeas93 on 16/03/17.
 */
public class MainFile {

    public static void main(String[] args) throws IOException {

        String dir = System.getProperty("user.dir");
        String path = dir + "/test.txt";
        String write_path = dir + "/output.txt";

        ReadFile file = new ReadFile(path);
        WriteFile writeFile = new WriteFile(write_path, true);

        // For simplicity only the starting year and the ending year are given as arguments
        // Paths can be added later on if needed

        // This is specific for the example in the Developer Problem
        if (args.length == 2) {
            int startYear = Integer.parseInt(args[0]);
            int endYear = Integer.parseInt(args[1]) + 1;
            List<Double> comp_entries = makeCalculations(file, "Comp", startYear, endYear);
            List<Double> non_comp_entries = makeCalculations(file, "Non-Comp", startYear, endYear);
            String line1 = String.valueOf(startYear).concat(",").concat(String.valueOf(endYear-startYear));
            writeFile.writeToFile(line1);
            String line2 = "Comp".concat(triangleEntriesString(comp_entries));
            writeFile.writeToFile(line2);
            String line3 = "Non-Comp".concat(triangleEntriesString(non_comp_entries));
            writeFile.writeToFile(line3);
        } else {
            System.out.println("Please use the format: java MainFile [productName] [startYear] [lastYear]");
        }

    }

    public static List<Double> makeCalculations(ReadFile file, String productName, int startYear, int endYear) throws IOException {
        // Get the data from the file in a List
        List<RowData> data = file.processFile();
        int number_years = file.numberOfDevelopmentYears(data);

        // Find the relevant rows for the product we want to make the cumulative triangle for
        List<RowData> product_data = file.processProduct(data, productName);

        Product product = new Product(product_data);

        // Loop over the years provided
        for (int i = startYear; i < endYear; i++) {
            double cumulative = 0.0;
            // Map of distinct pairs of origin year and pair of development year and incremental value
            Map<Integer, Map<Integer, Double>> map = file.getPairs(product_data, i);

            // Inverse loop to achieve the triangle
            for (int j = number_years; j > 0; j--) {
                // Check if map contains the origin year
                // This ensure that even if a year is skipped from the starting year a value is added in the entries
                if (map.containsKey(i)) {
                    // Check if there is a value for development year (endYear - j), given the origin year i
                    // and add it as an entry in the product
                    if (map.get(i).containsKey(endYear - j)) {
                        double incr = map.get(i).get(endYear - j);
                        double new_entry = file.addToCumulative(incr, cumulative);
                        cumulative += incr;
                        product.addEntry(new_entry);
                    } else { // Otherwise use the value 0.0 in the entry of the product
                        double incr = 0.0;
                        double new_entry = file.addToCumulative(incr, cumulative);
                        cumulative += incr;
                        product.addEntry(new_entry);
                    }
                } else {
                    product.addEntry(0.0);
                }
            }
            number_years--;
        }
        return product.getTriangleEntries();

    }

    public static String triangleEntriesString(List<Double> entries)  {
        String s = "";
        for (double d : entries) {
            s += "," + String.valueOf(d);
        }
        return s;
    }

}
