import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * Created by kendeas93 on 15/03/17.
 */
public class ReadFile {

    private String file_path;

    public ReadFile(String file_path) throws IOException {
        this.file_path = file_path;
    }

    public List<RowData> processFile() throws IOException {
        List<RowData> data = new ArrayList<>();
        FileReader fileReader = new FileReader(file_path);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;
        int line_number = 1;
        while ((line = bufferedReader.readLine()) != null) {
            String[] stringArray = line.split(",");
            double value = 0.0;
            if (line_number == 1) {
                // Headings
            } else {
                String product = stringArray[0];
                int origin_year = Integer.parseInt(stringArray[1].trim());
                int development_year = Integer.parseInt(stringArray[2].trim());
                // Check if incremental value is missing or not and set the value accordingly
                try {
                    value = Double.parseDouble(stringArray[3].trim());
                } catch (Exception e) {
                    value = 0.0;
                }
                data.add(new RowData(product, origin_year, development_year, value));
            }
            line_number++;
        }
        bufferedReader.close();
        return data;
    }
    
    public int numberOfDevelopmentYears(List<RowData> data) {
        int first_year = 2017;
        int last_year = 0;

        for (RowData rd : data) {
            if (rd.getOrigin_year() >= last_year) {
                last_year = rd.getOrigin_year();
            }
            if (rd.getOrigin_year() <= first_year) {
                first_year = rd.getOrigin_year();
            }
        }

        return (last_year - first_year) + 1;
    }

    public int numberOftriangleEntries(int numberOfYears) {
        // Sum of n series of numbers
        return (numberOfYears * (numberOfYears + 1)) / 2;
    }

    public List<RowData> processProduct(List<RowData> data, String searchProduct) {
        List<RowData> result = new ArrayList<>();

        for (RowData rd : data) {
            String product = rd.getProduct();
            if (product.equals(searchProduct)) {
                result.add(rd);
            }
        }
        return result;
    }

    public double addToCumulative(double incrValue, double cumulativeValue) {
        return cumulativeValue + incrValue;
    }

    // Map of pairs of origin year and pair of development year and increment value
    // Ensures distinct pairs of values
    public Map<Integer, Map<Integer, Double>> getPairs(List<RowData> data, int year) {
        Map<Integer, Double> inner = new HashMap<>();
        Map<Integer, Map<Integer, Double>> result = new HashMap<>();
        for (RowData rd : data) {
            if (rd.getOrigin_year() == year) {
                inner.put(rd.getDevelop_year(), rd.getIncrem_value());
                result.put(rd.getOrigin_year(), inner);
            }
        }
        return result;
    }
}

