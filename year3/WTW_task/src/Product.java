import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by kendeas93 on 16/03/17.
 */
public class Product {

    private List<RowData> data;
    private List<Double> triangleEntries = new ArrayList<>();

    public Product(List<RowData> data) {
        this.data = data;
    }

    public void addEntry(double value) {
        this.triangleEntries.add(value);
    }

    public List<Double> getTriangleEntries() {
        return triangleEntries;
    }

    @Override
    public String toString() {
        return "Product{" +
                "data=" + data +
                ", triangleEntries=" + triangleEntries +
                '}';
    }
}
