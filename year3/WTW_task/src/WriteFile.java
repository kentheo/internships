import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * Created by kendeas93 on 17/03/17.
 */
public class WriteFile {

    private String path;
    private boolean append = false;

    public WriteFile(String path) {
        this.path = path;
    }

    public WriteFile(String path, boolean append) {
        this.path = path;
        this.append = append;
    }

    public void writeToFile(String text) throws IOException {
        FileWriter writer = new FileWriter(path, append);
        PrintWriter print_text = new PrintWriter(writer);

        print_text.printf("%s" + "%n", text);
        print_text.close();
    }
}
